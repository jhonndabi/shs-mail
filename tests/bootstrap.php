<?php

chdir(dirname(__DIR__));

require 'vendor/autoload.php';

// Disable garbage collector to prevent segfaults
gc_disable();
